describe('Level 3', () => {

  it('Visits the "Level 3" page', () => {
    cy.visit('http://localhost:4200/');
    cy.contains('Level 3').click();
    cy.url().should('include', '/purchase-level-3');
  })

  it ('AmountFormControl component should work properly', () => {
    cy.get('wdg-amount-form-control').should('be.visible');
    cy.contains('Valider votre montant').should('have.attr', 'disabled');
    cy.contains('+').click();
    cy.contains('Valider votre montant').should('not.have.attr', 'disabled');
    cy.get('input[formControlName="amount"]').clear();
    cy.get('input[formControlName="amount"]').type('28');
    cy.contains('Rechercher').click();
    cy.contains('Valider votre montant').should('have.attr', 'disabled');
    cy.contains('Montant inférieur le plus proche : 26 €').should('be.visible');
    cy.contains('Sélectionner 26 €').should('be.visible');
    cy.contains('Montant supérieur le plus proche : 35 €').should('be.visible');
    cy.contains('Sélectionner 35 €').should('be.visible');
    cy.contains('Sélectionner 35 €').click();
    cy.get('input[formControlName="amount"]').should('have.value', '35');
    cy.contains('Valider votre montant').should('not.have.attr', 'disabled');
    cy.contains('Valider votre montant').click();
    cy.contains('Vous avez choisi ce montant : 35 €').should('be.visible');
  })

})
