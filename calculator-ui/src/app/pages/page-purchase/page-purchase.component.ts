import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { AmountComponent } from './../../components/amount/amount.component';
import { WdgUserService } from './../../services/wdg-user.service';
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

@Component({
  selector: 'wdg-page-purchase',
  templateUrl: './page-purchase.component.html',
  styleUrls: ['./page-purchase.component.scss']
})
export class PagePurchaseComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('wdgAmount') wdgAmount!: AmountComponent;

  public get shopId(): number {
    return this.wdgUserService.shopId;
  }

  private _subscription: Subscription = new Subscription();

  constructor(
    private wdgUserService: WdgUserService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    if (this.wdgAmount) {
      this._subscription.add(
        this.wdgAmount.validAmount.subscribe((amount) => {
          this.snackBar.open(`Vous avez choisi ce montant : ${amount} €`, undefined, {
            panelClass: 'wdg-success-snackbar',
            duration: 2000,
            verticalPosition: 'top',
          });
        })
      );
    }
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

}
