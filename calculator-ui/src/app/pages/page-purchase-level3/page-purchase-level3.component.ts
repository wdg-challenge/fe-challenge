import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { AmountComponent } from './../../components/amount/amount.component';
import { WdgUserService } from './../../services/wdg-user.service';
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

@Component({
  selector: 'wdg-page-purchase-level3',
  templateUrl: './page-purchase-level3.component.html',
  styleUrls: ['./page-purchase-level3.component.scss']
})
export class PagePurchaseLevel3Component implements OnInit {

  public get shopId(): number {
    return this.wdgUserService.shopId;
  }
  public purchaseForm = new FormGroup({
    amount: new FormControl({value: 0, cards: []})
  });

  constructor(
    private wdgUserService: WdgUserService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
  }

  submitPurchaseForm() {
    console.log(this.purchaseForm.value);
    this.snackBar.open(`Vous avez choisi ce montant : ${this.purchaseForm.value.amount.value} €`, undefined, {
      panelClass: 'wdg-success-snackbar',
      duration: 2000,
      verticalPosition: 'top',
    });
  }

  toggleAmountFormControlDisable() {
    if (this.purchaseForm.controls['amount'].disabled) {
      this.purchaseForm.controls['amount'].enable();
    } else {
      this.purchaseForm.controls['amount'].disable();
    }
  }

}
