import { TokenInterceptor } from './interceptors/token.interceptor';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { PageHomeComponent } from './pages/page-home/page-home.component';
import { HeaderComponent } from './components/header/header.component';
import { PageContainerComponent } from './components/page-container/page-container.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PagePurchaseComponent } from './pages/page-purchase/page-purchase.component';
import { AmountComponent } from './components/amount/amount.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { ButtonComponent } from './components/button/button.component';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { CardComponent } from './components/card/card.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CardsComponent } from './components/cards/cards.component';
import { PagePurchaseLevel3Component } from './pages/page-purchase-level3/page-purchase-level3.component';
import { AmountFormControlComponent } from './components/amount-form-control/amount-form-control.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    PageHomeComponent,
    HeaderComponent,
    PageContainerComponent,
    PagePurchaseComponent,
    AmountComponent,
    ButtonComponent,
    CardComponent,
    CardsComponent,
    PagePurchaseLevel3Component,
    AmountFormControlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatRippleModule,
    MatIconModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
