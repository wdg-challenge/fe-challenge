import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class WdgErrorManagerService {
  constructor(private snackBar: MatSnackBar) {}

  public manageHttpError(error: HttpErrorResponse, message?: string) {
    this.snackBar.open(message ? message : this.getMessageFromError(error), undefined, {
      panelClass: 'wdg-error-snackbar',
      duration: 2000,
      verticalPosition: 'top',
    });
  }

  private getMessageFromError(error: HttpErrorResponse): string {
    switch (error.status) {
      case 401:
        return `Vous n'êtes pas authentifié.`
      case 403:
        return `L'accès est refusé.`
      case 404:
        return `La ressource est introuvable.`
      default:
        return `Une erreur s'est produite.`;
    }
  }
}
