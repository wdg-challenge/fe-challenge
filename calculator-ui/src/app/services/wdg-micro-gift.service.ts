import { Combinations } from './../models/Combinations';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * MicroGiftApi methods
 */
@Injectable({
  providedIn: 'root'
})
export class WdgMicroGiftService {

  private _shopUrl = `${environment.apiBaseUrl}shop`;

  constructor(private http: HttpClient) { }

  public shopSearchCombination(shopId: number, amount: number): Observable<Combinations> {
    return this.http.get<Combinations>(`${this._shopUrl}/${shopId}/search-combination`, {params: {amount}});
  }
}
