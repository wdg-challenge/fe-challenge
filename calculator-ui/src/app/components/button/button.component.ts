import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'wdg-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() size: 'small' | 'medium' | 'big' = 'small';
  @Input() disabled = false;

  constructor() { }

  ngOnInit(): void {
  }

}
