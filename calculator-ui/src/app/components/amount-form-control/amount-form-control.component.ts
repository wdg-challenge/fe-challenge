import { WdgErrorManagerService } from './../../services/wdg-error-manager.service';
import { Combinations } from './../../models/Combinations';
import { WdgMicroGiftService } from './../../services/wdg-micro-gift.service';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CalculatorComponentValue } from 'src/app/interfaces/CalculatorComponentValue';
import { Combination } from 'src/app/models/Combination';

@Component({
  selector: 'wdg-amount-form-control',
  templateUrl: './amount-form-control.component.html',
  styleUrls: ['./amount-form-control.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi:true,
      useExisting: AmountFormControlComponent
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: AmountFormControlComponent
    }
  ]
})
export class AmountFormControlComponent implements OnInit, OnDestroy, ControlValueAccessor {
  @Input() shopId!: number;

  @Output() validAmount = new EventEmitter<number>();

  public amountForm = new FormGroup({
    amount: new FormControl(0, Validators.required),
  });
  public combinations!: Combinations;
  public amountIsValid = false;
  public disabled = false;
  public touched = false;

  private _subscription: Subscription = new Subscription();

  constructor(
    private wdgMicroGiftService: WdgMicroGiftService,
    private wdgErrorManagerService: WdgErrorManagerService
  ) {}

  onChange = (ccv: CalculatorComponentValue) => {};

  onTouched = () => {};

  writeValue(ccv: CalculatorComponentValue): void {
    this.amountForm.controls['amount'].setValue(ccv.value);
    if (ccv.cards && ccv.cards.length) {
      this.combinations = new Combinations(new Combination(ccv.value, ccv.cards));
    }
  }

  registerOnChange(onChange: any): void {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (this.amountIsValid) {
      return null;
    } else {
      return {
        mustBeAValidCombination: {
          combination: control.value
        }
      };
    }
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  /**
   * Submit amountForm with amount value
   */
  public submitAmountForm(): void {
    this.markAsTouched();
    if (this.amountForm.valid && this.shopId) {
      this._subscription.add(
        this.wdgMicroGiftService
          .shopSearchCombination(
            this.shopId,
            this.amountForm.controls['amount'].value
          )
          .subscribe({
            next: (c) => {
              console.log(c);
              this.combinations = c;
              if (c.equal) {
                this.amountIsValid = true;
                this.onChange({
                  value: c.equal.value,
                  cards: c.equal.cards
                });
              } else {
                this.amountIsValid = false;
                this.onChange({
                  value: this.amountForm.controls['amount'].value,
                  cards: []
                });
              }
            },
            error: (e) => this.wdgErrorManagerService.manageHttpError(e),
          })
      );
    } else {
      this.amountForm.markAllAsTouched();
    }
  }

  /**
   * Update amout form with new value
   */
  updateAmount(newAmount: number) {
    this.amountForm.controls['amount'].setValue(newAmount);
    this.submitAmountForm();
  }

  /**
   * Search next combination then call api with it
   */
  selectNextCombination() {
    this.markAsTouched();
    this._subscription.add(
      this.wdgMicroGiftService
        .shopSearchCombination(
          this.shopId,
          this.amountForm.controls['amount'].value + 1
        )
        .subscribe({
          next: (c) => {
            console.log(c);
            if (c.equal) {
              this.amountForm.controls['amount'].setValue(c.equal.value);
              this.selectNextCombination();
            } else {
              if (c.ceil) {
                this.amountForm.controls['amount'].setValue(c.ceil.value);
                this.submitAmountForm();
              }
            }
          },
          error: (e) => this.wdgErrorManagerService.manageHttpError(e),
        })
    );
  }

  /**
   * Search previous combination then call api with it
   */
  selectPrevCombination() {
    this.markAsTouched();
    this._subscription.add(
      this.wdgMicroGiftService
        .shopSearchCombination(
          this.shopId,
          this.amountForm.controls['amount'].value - 1
        )
        .subscribe({
          next: (c) => {
            console.log(c);
            if (c.equal) {
              this.amountForm.controls['amount'].setValue(c.equal.value);
              this.selectPrevCombination();
            } else {
              if (c.floor) {
                this.amountForm.controls['amount'].setValue(c.floor.value);
                this.submitAmountForm();
              }
            }
          },
          error: (e) => this.wdgErrorManagerService.manageHttpError(e),
        })
    );
  }
}
