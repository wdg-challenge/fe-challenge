import { WdgErrorManagerService } from './../../services/wdg-error-manager.service';
import { Combinations } from './../../models/Combinations';
import { WdgMicroGiftService } from './../../services/wdg-micro-gift.service';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'wdg-amount',
  templateUrl: './amount.component.html',
  styleUrls: ['./amount.component.scss'],
})
export class AmountComponent implements OnInit, OnDestroy {
  @Input() shopId!: number;

  @Output() validAmount = new EventEmitter<number>();

  public amountForm = new FormGroup({
    amount: new FormControl(0, Validators.required),
  });
  public combinations!: Combinations;
  public amountIsValid = false;

  private _subscription: Subscription = new Subscription();
  private _lastValidAmount?: number;

  constructor(
    private wdgMicroGiftService: WdgMicroGiftService,
    private wdgErrorManagerService: WdgErrorManagerService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  /**
   * Submit amountForm with amount value
   */
  public submitAmountForm(): void {
    if (this.amountForm.valid && this.shopId) {
      this._subscription.add(
        this.wdgMicroGiftService
          .shopSearchCombination(
            this.shopId,
            this.amountForm.controls['amount'].value
          )
          .subscribe({
            next: (c) => {
              console.log(c);
              this.combinations = c;
              if (c.equal) {
                this.amountIsValid = true;
                this._lastValidAmount = c.equal.value;
              } else {
                this.amountIsValid = false;
              }
            },
            error: (e) => this.wdgErrorManagerService.manageHttpError(e),
          })
      );
    } else {
      this.amountForm.markAllAsTouched();
    }
  }

  /**
   * Update amout form with new value
   */
  updateAmount(newAmount: number) {
    this.amountForm.controls['amount'].setValue(newAmount);
    this.submitAmountForm();
  }

  /**
   * Search next combination then call api with it
   */
  selectNextCombination() {
    this._subscription.add(
      this.wdgMicroGiftService
        .shopSearchCombination(
          this.shopId,
          this.amountForm.controls['amount'].value + 1
        )
        .subscribe({
          next: (c) => {
            console.log(c);
            if (c.equal) {
              this.amountForm.controls['amount'].setValue(c.equal.value);
              this.selectNextCombination();
            } else {
              if (c.ceil) {
                this.amountForm.controls['amount'].setValue(c.ceil.value);
                this.submitAmountForm();
              }
            }
          },
          error: (e) => this.wdgErrorManagerService.manageHttpError(e),
        })
    );
  }

  /**
   * Search previous combination then call api with it
   */
  selectPrevCombination() {
    this._subscription.add(
      this.wdgMicroGiftService
        .shopSearchCombination(
          this.shopId,
          this.amountForm.controls['amount'].value - 1
        )
        .subscribe({
          next: (c) => {
            console.log(c);
            if (c.equal) {
              this.amountForm.controls['amount'].setValue(c.equal.value);
              this.selectPrevCombination();
            } else {
              if (c.floor) {
                this.amountForm.controls['amount'].setValue(c.floor.value);
                this.submitAmountForm();
              }
            }
          },
          error: (e) => this.wdgErrorManagerService.manageHttpError(e),
        })
    );
  }

  /**
   * Trigger validAmount Output
   */
   validateAmount() {
     if (this.amountIsValid) {
      this.validAmount.emit(this._lastValidAmount);
     }
   }
}
