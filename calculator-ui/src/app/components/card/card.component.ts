import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wdg-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() value!: number;

  constructor() { }

  ngOnInit(): void {
  }

}
