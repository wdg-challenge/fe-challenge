import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Combination } from 'src/app/models/Combination';

@Component({
  selector: 'wdg-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  @Input() combination!: Combination;
  @Input() label!: string;
  @Input() displaySelectBtn = false;

  @Output() selectAmount = new EventEmitter<number>();

  public totalAmount = 0;

  constructor() { }

  ngOnInit(): void {}

}
