import { Component } from '@angular/core';

@Component({
  selector: 'wdg-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'calculator-ui';
}
