import { PagePurchaseLevel3Component } from './pages/page-purchase-level3/page-purchase-level3.component';
import { PagePurchaseComponent } from './pages/page-purchase/page-purchase.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { PageHomeComponent } from './pages/page-home/page-home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: PageHomeComponent },
  { path: 'purchase', component: PagePurchaseComponent },
  { path: 'purchase-level-3', component: PagePurchaseLevel3Component },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
