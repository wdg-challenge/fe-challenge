import { Combination } from './Combination';
export class Combinations {
  equal?: Combination;
  floor?: Combination;
  ceil?: Combination;
  constructor(equal?: Combination, floor?: Combination, ceil?: Combination) {
    this.equal = equal;
    this.floor = floor;
    this.ceil = ceil;
  }
}
