export class Combination {
  value: number;
  cards: number[] = [];

  constructor(value: number, cards: number[]) {
    this.value = value;
    this.cards = cards;
  }
}
