# fe-challenge



## Au sujet des 3 exercices

Les deux premiers exercices ont été réalisés avec le même composant AmountComponent.
Vous pouvez y accéder sur la page Level 1 & 2.

Pour le troisième exercice j'ai dupliqué le composant précédent sous le nom AmountFormControlComponent avant d'effectuer la refacto.
Vous pouvez y accéder sur la page Level 3.

## Note sur les tests

Je n'ai pas mis en place de tests unitaires, mais des tests d'intégration réalisés avec l'outil Cypress.

Pour lancer les tests, après avoir démarré l'api et l'application Angular, executez la commande suivante à la racine de l'application calculator-ui :

```
npm run cypress:open
```
Lorsque la fenêtre Cypress est apparue, cliquez sur Run 2 integration specs pour lancer les tests
